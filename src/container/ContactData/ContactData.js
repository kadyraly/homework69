import React, {Component} from 'react';
import {connect} from 'react-redux';


import './ContactData.css';
import Button from "../../component/UI/Button/Button";
import {placeOrder} from "../../store/action/order";





class ContactData extends Component {
    state = {
        name: '',
        email: '',
        street: '',
        postal: ''
    };

    valueChanged = (event) => {
        this.setState({[event.target.name]: event.target.value});
    };

    orderHandler = event => {
        event.preventDefault();

        const order = {
            dish: this.props.dish,
            price: this.props.price,
            customer: {
                name: this.state.name,
                email: this.state.email,
                street: this.state.street,
                postal: this.state.postal
            }
        };

        this.props.onPlaceOrder(order);
    };

    render() {
       return (
           <div className="ContactData">

            <form>
                <input className="Input" type="text" name="name" placeholder="Your Name"
                       value={this.state.name} onChange={this.valueChanged} />
                <input className="Input" type="email" name="email" placeholder="Your Mail"
                       value={this.state.email} onChange={this.valueChanged} />
                <input className="Input" type="text" name="street" placeholder="Street"
                       value={this.state.street} onChange={this.valueChanged} />
                <input className="Input" type="text" name="postal" placeholder="Postal Code"
                       value={this.state.postal} onChange={this.valueChanged} />
                <Button clicked={this.orderHandler} btnType="Success">ORDER</Button>
            </form>
           </div>
        );




    }
}
const mapStateToProps = state => {
    return {
        dish: state.contact.dish,
        price: state.contact.totalPrice,
        loading: state.order.loading,
        ordered: state.order.ordered
    };
};

const mapDispatchToProps = dispatch => {
    return {
        onPlaceOrder: (dish, price) => dispatch(placeOrder(dish, price)),

    };

};

export default connect(mapStateToProps, mapDispatchToProps)(ContactData);