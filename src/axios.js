import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://home-1d652.firebaseio.com/'
});


export default instance;