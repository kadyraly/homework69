import React, {Component} from 'react';
import './OrderList.css';
import {connect} from 'react-redux';
import {addDish, getDish} from "../../store/action/dishes";



class OrderList extends Component {

    componentDidMount () {
        this.props.onDishGot();
    }

    render () {
        return(
            <div className="dishes">
                {Object.keys(this.props.dishes).map(dish => (
                    <div className="dishName">
                        <img className="img" src={this.props.dishes[dish].img} alt=""/>
                        <strong>{dish}: </strong>
                        <p>Price:{this.props.dishes[dish].price}</p>
                       <button onClick={() => this.props.onDishAdded(dish)}>Add to cart</button>
                    </div>
                ))}

            </div>
        )
    }
};

const mapStateToProps = state => {
    return {
        dishes: state.order.dishes,
        price: state.order.totalPrice

    };
};
const mapDispatchToProps = dispatch => {
    return {
        onDishAdded: dish => dispatch(addDish(dish)),
        onDishGot: () => dispatch(getDish())

    };
};


export default connect(mapStateToProps, mapDispatchToProps)(OrderList);