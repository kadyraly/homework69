import * as actionTypes from '../action/actionTypes';



const initialState = {
    dishes: {},
};




const reducer = (state = initialState, action) => {

    switch (action.type) {

        case actionTypes.ORDER_SUCCESS:
            return {...state, dishes: action.value};



        default:
            return state;
    }

};

export default reducer;

