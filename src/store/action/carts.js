import * as actionTypes from './actionTypes';



export const removeDish = dish => {
    return{type: actionTypes.DISH_REMOVE, dish}
};
