export const GET_DISH = 'GET_DISH';
export const ADD_DISH = 'ADD_DISH';
export const DISH_REMOVE = 'DISH_REMOVE';
export const ORDER_PLACE = 'ORDER_PLACE';

export const ORDER_REQUEST = 'ORDER_REQUEST';
export const ORDER_SUCCESS = 'ORDER_SUCCESS';
export const ORDER_FAILURE = 'ORDER_FAILURE';

