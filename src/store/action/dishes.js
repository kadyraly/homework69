import * as actionTypes from './actionTypes';

import axios from "../../axios";

export const orderRequest = () => {
    return {type: actionTypes.ORDER_REQUEST};
};

export const orderSuccess = (value) => {
    return {type: actionTypes.ORDER_SUCCESS, value};
};

export const orderFailure = error => {
    return {type: actionTypes.ORDER_FAILURE, error};
};

export const getDish = () => {
    return dispatch => {
        dispatch(orderRequest());
        axios.get('/dishes.json').then(response => {
            dispatch(orderSuccess(response.data));
        }, error => {
            dispatch(orderFailure(error))
        })
    }
};

export const addDish = (dish, price) => {
    return  {type: actionTypes.ADD_DISH, dish, price}

};


