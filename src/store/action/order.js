import * as actionTypes from './actionTypes';
import axios from '../../axios';

export const orderRequest = () => {
    return {type: actionTypes.ORDER_REQUEST};
};

export const orderSuccess = () => {
    return {type: actionTypes.ORDER_SUCCESS};
};

export const orderFailure = error => {
    return {type: actionTypes.ORDER_FAILURE, error};
};

export const placeOrder = (dish, price) => {
    return dispatch => {
        dispatch(orderRequest());
        axios.post('/dishes.json', dish, price).then(() => {
            dispatch(orderSuccess());
        }, error => {
            dispatch(orderFailure(error))
        })
    }
};
