import * as actionTypes from '../action/actionTypes';



const initialState = {
    dish: {},
    price: 0
};




const reducer = (state = initialState, action) => {

    switch (action.type) {

        case actionTypes.ORDER_SUCCESS:
            return {...state, dish: action.dish, price: action.price};



        default:
            return state;
    }

};

export default reducer;
