import * as actionTypes from '../action/actionTypes';
import prices from '../../prices';

const initialState = {
    dish: {},
    totalPrice: 0,

    loading: false,
    error: null,
    ordered: false,

};

const reducer = (state = initialState, action) => {
    switch (action.type) {

        case actionTypes.ORDER_REQUEST:
            return {...state, loading: true, error: null};
        case actionTypes.ORDER_SUCCESS:
            return {...state, loading: false, error: null, ordered: true};
        case actionTypes.ORDER_FAILURE:
            return {...state, loading: false, error: state.error};


        case actionTypes.ADD_DISH:
            const dishes = {...state.dish};
            if(dishes[action.dish]) {
                dishes[action.dish]++;
            } else {
                dishes[action.dish] = 1;
            }

            return {...state, dish: dishes, totalPrice: state.totalPrice + prices[action.dish]};
        case actionTypes.DISH_REMOVE:

            const foods = {...state.dish};
            if(foods[action.dish]) {
                foods[action.dish]--;
            } else {
                foods[action.dish] = 1;
            }

            return {...state, dish: foods, totalPrice: state.totalPrice - prices[action.dish]};
        default:
            return state;
    }
};

export default reducer;