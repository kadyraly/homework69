import React, { Component } from 'react';

import './App.css';


import OrderList from "./container/OrderList/OrderList";
import PlaceList from "./container/PlaceList/PlaceList";
import ContactData from "./container/ContactData/ContactData";

class App extends Component {
  render() {
    return (
      <div className="App">
       <OrderList />
          <PlaceList />
          <ContactData />
      </div>
    );
  }
}

export default App;
