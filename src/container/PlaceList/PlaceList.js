import React, {Component} from 'react';
import prices from '../../prices';

import {connect} from 'react-redux';
import './PlaceList.css';
import {removeDish} from "../../store/action/carts";
import {placeOrder} from "../../store/action/order";





class PlaceList extends Component {

    componentDidMount () {

    }

    render () {

        return(
            <div className="PlayList">

                    <div className="dishName">

                        {Object.keys(this.props.dish).map(dishName => {

                            return (
                            <div className="dishName">

                                <strong>{dishName}: </strong>
                                <p>Price:{prices[dishName] * this.props.dish[dishName]}</p>
                                <button onClick={() => this.props.onDishRemoved(dishName)}>x</button>
                            </div>
                        )})}
                    </div>

                <strong>Total price:{this.props.price} </strong>
                <button onClick={() => this.props.onPlaceOrder()}>Place order</button>
            </div>
        )
    }
};

const mapStateToProps = state => {
    return {

        dish: state.bb.dish,
        price: state.bb.totalPrice

    };
};

const mapDispatchToProps = dispatch => {
    return {

        onDishRemoved: dish => dispatch(removeDish(dish)),
        onPlaceOrder: (dish, price) => dispatch(placeOrder(dish, price))
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(PlaceList);